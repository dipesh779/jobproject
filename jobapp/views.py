from django.shortcuts import render, redirect
from django .views.generic import *
from .models import *
from .forms import *
from django.contrib. auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.urls import reverse, reverse_lazy


class EmployerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.filter(name="employer").exists:
            pass
        else:
            return redirect("/job/login/")
        return super().dispatch(request, *args, **kwargs)


class JobSeekerHomeView(TemplateView):
    template_name = "jobseekertemplates/jobseekerhome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['sliders'] = JobSlider.objects.all()
        return context


class EmployerHomeView(EmployerRequiredMixin, TemplateView):
    template_name = "employertemplates/employerhome.html"


class AdminHomeView(TemplateView):
    template_name = "admintemplates/adminhome.html"


class JobListView(ListView):
    template_name = "jobseekertemplates/joblist.html"
    queryset = Job.objects.all()
    context_object_name = "jobs"


class JobDetailView(DetailView):
    template_name = "jobseekertemplates/jobdetail.html"
    model = Job
    context_object_name = "job"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        job_id = self.kwargs["pk"]
        job = Job.objects.get(id=job_id)
        job.view_count += 1
        job.save()
        return context


class JobSeekerRegistrationView(CreateView):
    template_name = "jobseekertemplates/jobseekerregistration.html"
    form_class = JobSeekerForm
    success_url = "/"

    def form_valid(self, form):
        u_name = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]
        user = User.objects.create_user(u_name, "", pword)
        form.instance.user = user
        login(self.request, user)
        return super().form_valid(form)


class JobSeekerJobApplyView(CreateView):
    template_name = "jobseekertemplates/jobseekerjobapply.html"
    form_class = JobApplyForm
    success_url = "/"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.groups.first().name == "jobseeker":
            pass
        else:
            return redirect("/job/login/")

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        job_id = self.kwargs["pk"]
        job = Job.objects.get(id=job_id)
        user = self.request.user
        job_seeker = Job_Seeker.objects.get(user=user)
        form.instance.job = job
        form.instance.jobseeker = job_seeker
        return super().form_valid(form)


class JobLoginView(FormView):
    template_name = "jobseekertemplates/joblogin.html"
    form_class = JobLoginForm
    success_url = "/"

    def form_valid(self, form):
        u_name = form.cleaned_data["username"]
        p_word = form.cleaned_data["password"]

        user = authenticate(username=u_name, password=p_word)
        self.thisuser = user

        if user is not None and user.groups.exists():
            login(self.request, user)

        else:
            return render(self.request, self.template_name,
                          {"error": "username or password didn't match", "form": form})

        return super().form_valid(form)

    def get_success_url(self):
        if self.thisuser.groups.filter(name="jobseeker").exists():
            return reverse("jobapp:jobseekerhome")
        elif self.thisuser.groups.filter(name="employer").exists():
            return reverse("jobapp:employerhome")
        else:
            return reverse("jobapp:login")


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/job/login/")


class JobSeekerProfileView(TemplateView):
    template_name = "jobseekertemplates/jobseekerprofile.html"

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if user.is_authenticated and user.groups.filter(name="jobseeker").exists:
            pass

        else:
            return redirect("/job/login/")
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        jobseeker = Job_Seeker.objects.get(user=logged_user)
        context["jobseeker"] = jobseeker

        return context


class EmployerRegistrationView(CreateView):
    template_name = "employertemplates/employerregistration.html"
    form_class = EmployerForm
    success_url = "/"

    def form_valid(self, form):
        u_name = form.cleaned_data["username"]
        p_word = form.cleaned_data["password"]
        user = User.objects.create_user(u_name, "", p_word)
        form.instance.user = user
        return super().form_valid(form)


class EmployerProfileView(EmployerRequiredMixin, TemplateView):
    template_name = "employertemplates/employerprofile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        employer = Employer.objects.get(user=self.request.user)
        context["employer"] = employer
        return context


class EmployerJobDetailView(EmployerRequiredMixin, DetailView):
    template_name = "employertemplates/employerjobdetail.html"
    model = Job
    context_object_name = "jobobject"


class EmployerJobCreateView(EmployerRequiredMixin, CreateView):
    template_name = "employertemplates/employerjobcreate.html"
    form_class = EmployerJobForm
    success_url = reverse_lazy("jobapp:employerprofile")

    def form_valid(self, form):
        employer = Employer.objects.get(user=self.request.user)
        form.instance.employer = employer
        return super().form_valid(form)
