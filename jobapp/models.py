from django.db import models
from django.contrib.auth.models import User, Group


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Admin(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    email = models.EmailField()
    address = models.CharField(max_length=200)
    image = models.ImageField(upload_to="admins")

    def __str__(self):
        return self.name


class Employer(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    email = models.EmailField()
    company = models.CharField(max_length=100)
    website = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    image = models.ImageField(upload_to="employer")
    company_image = models.ImageField(upload_to="employer")

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name="employer")
        self.user.groups.add(group)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Job_Seeker(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    email = models.EmailField()
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    image = models.ImageField(upload_to="job_seeker")
    qualification = models.TextField()
    skills = models.TextField()
    about = models.TextField()
    cv = models.FileField(upload_to="job_seeker")

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name="jobseeker")
        self.user.groups.add(group)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class JobCategory(TimeStamp):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to="jobcategory")

    def __str__(self):
        return self.title


JOB_TYPE = (
    ("full_time", "full time"),
    ("part_time", "part time"),
    ("contract", "contract"),
    ("internship", "internship"),
)
LEVEL = (
    ("begineer", "begineer"),
    ("junior", "junior"),
    ("mid", "mid"),
    ("senior", "senior"),
)


class Job(TimeStamp):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to="job")
    category = models.ForeignKey(JobCategory, on_delete=models.CASCADE)
    job_type = models.CharField(max_length=50, choices=JOB_TYPE)
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    salary = models.CharField(max_length=100)
    level = models.CharField(max_length=50, choices=LEVEL)
    deadline = models.DateTimeField()
    skills = models.TextField()
    vacancy_number = models.PositiveIntegerField()
    education = models.CharField(max_length=200)
    details = models.TextField()
    view_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title


class JobApplication(TimeStamp):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    jobseeker = models.ForeignKey(Job_Seeker, on_delete=models.CASCADE)
    cover_letter = models.FileField(upload_to="job_application")

    def __str__(self):
        return self.jobseeker.name


class JobSlider(models.Model):
    title = models.CharField(max_length=100)
    caption = models.CharField(max_length=300)
    image = models.ImageField(upload_to="Slider")
    link = models.CharField(max_length=500)

    def __str__(self):
        return self.title
