from django.contrib import admin
from .models import *


admin.site.register([Admin, Employer, Job_Seeker,
                     JobCategory, Job, JobApplication, JobSlider])
# Register your models here.
