from django import forms
from .models import *
from django.contrib.auth.models import User


class JobSeekerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput)
    password = forms.CharField(widget=forms.PasswordInput)
    confirm_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = Job_Seeker
        # exclude = ["user"]
        fields = ["username", "password", "confirm_password", "email", "name", "address",
                  "mobile", "image", "qualification", "skills", "about", "cv"]

    def clean_username(self):
        username = self.cleaned_data.get("username")
        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                "jobseeker with this username already exist")

        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get("password")
        c_password = self.cleaned_data.get("confirm_password")
        if password != c_password:
            raise forms.ValidationError("password didn't match")

        return c_password


class JobApplyForm(forms.ModelForm):
    class Meta:
        model = JobApplication
        fields = ["cover_letter"]


class JobLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


class EmployerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Employer
        fields = ["username", "password", "confirm_password", "name", "mobile", "email", "company", "website",
                  "address", "image", "company_image"]

    def clean_username(self):
        username = self.cleaned_data.get("username")
        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                "employer with this username already exist")

        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get("password")
        c_password = self.cleaned_data.get("confirm_password")
        if password != c_password:
            raise forms.ValidationError("password didn't match")

        return c_password


class EmployerJobForm(forms.ModelForm):
    class Meta:
        model = Job
        exclude = ["employer"]
        widgets = {
            'title': forms.TextInput(attrs={"placeholder": "enter job title here...", "id": "jobtitle"}),
            'category': forms.Select(attrs={'id': "job category", 'id': "form-control"})
        }
