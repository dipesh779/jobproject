# Generated by Django 2.2 on 2019-04-19 06:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('jobapp', '0003_job_seeker_email'),
    ]

    operations = [
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=100)),
                ('caption', models.CharField(max_length=300)),
                ('image', models.ImageField(upload_to='Slider')),
                ('link', models.CharField(max_length=500)),
            ],
        ),
    ]
